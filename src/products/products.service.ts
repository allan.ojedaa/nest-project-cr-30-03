import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product,ProductDocument } from './schemas/product.schema';

@Injectable()
export class ProductsService {
  constructor(@InjectModel(Product.name) private productModel: Model<ProductDocument>){
    
  }
  async create(createProductDto: CreateProductDto):Promise<Product> {
    const createdCat = new this.productModel(createProductDto);
    return createdCat.save();
  }
  async findAll(fecha: Date): Promise<Product[]> {

    return this.productModel.find({
      date_created:{$gte:fecha}
  }).exec();
  }


}
