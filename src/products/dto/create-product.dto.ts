import { IsString, IsNumber, IsPositive, IsDateString, Allow } from 'class-validator';

import {ApiProperty} from '@nestjs/swagger';
import { All } from '@nestjs/common';
export class CreateProductDto {
    @IsString()
    @ApiProperty({
        type:String,
        required:true,
        example:'Nombre completo'
    })
    name: string;

    @IsNumber()
    @IsPositive()
    @ApiProperty({
        type:Number,
        required:true,
        example:'180.00',
    })
    price: number;

    @IsDateString()
    @ApiProperty({
        type: Date,
        required:true,
        example:'2021/03/22'
    })
    @Allow()
    @IsDateString()
    date_created: Date;
}
