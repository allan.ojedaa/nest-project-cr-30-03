const moment = require('moment');

function ConvertirFechaF(fecha:string){
    const IsoDateTo = moment(fecha,'YYYY-MM-DD').format('YYYY/MM/DD');
    return IsoDateTo;
}

export {ConvertirFechaF}