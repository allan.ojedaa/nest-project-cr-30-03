import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import {ConvertirFechaF} from './helpers/validar-fecha';
import { ApiOperation,ApiQuery,ApiBody } from '@nestjs/swagger';
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  @ApiBody({ description: 'Datos para un nuevo producto', type: CreateProductDto })
  @ApiOperation({summary:'Creación de producto'})
  create(@Body() createProductDto: CreateProductDto) {
    return this.productsService.create(createProductDto);
  }


  @Get(':date_created')
  @ApiQuery({name: 'date_created', description: 'Fecha de creación', type: Date, example: "2021-03-30" })
  @ApiOperation({summary:'Devuelve productos con una fecha de creación a partir de la proporcionada'})
  findAll(@Param('date_created') fecha: string) {
    return this.productsService.findAll(ConvertirFechaF(fecha));
  }

}
