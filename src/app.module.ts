import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
@Module({
  imports: [ProductsModule,
    ConfigModule.forRoot({
    isGlobal: true,
    envFilePath: '.development.env',
    }),
    MongooseModule.forRoot('mongodb+srv://'+process.env.USER+':'+process.env.PASS+'@cluster0.vicxa.mongodb.net/'+process.env.DB, {
    useNewUrlParser: true,
    useUnifiedTopology:true,
    useCreateIndex:true,
    useFindAndModify:false})
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
